<?php

namespace Database\Factories;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ParentModelFactory extends Factory
{
    protected $model = ParentModel::class;

    public function definition()
    {
        return [
            'fio' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
        ];
    }
}
