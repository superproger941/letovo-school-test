<?php

namespace Database\Factories;

use App\Models\Pupil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PupilFactory extends Factory
{
    protected $model = Pupil::class;

    public function definition()
    {
        return [
            'fio' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'address' => $this->faker->address,
        ];
    }
}
