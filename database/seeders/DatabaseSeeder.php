<?php

namespace Database\Seeders;

use App\Models\ParentModel;
use App\Models\Pupil;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Pupil::factory(100)->create();
        ParentModel::factory(5)->create();
    }
}
