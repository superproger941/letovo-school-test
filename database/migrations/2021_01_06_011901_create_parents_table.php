<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParentsTable extends Migration
{
    public function up()
    {
        //"ФИО", "мобильный телефон";
        Schema::create('parents', function (Blueprint $table) {
            $table->id();
            $table->string('fio');
            $table->string('phone');
            $table->timestamps();
        });

        Schema::create('pupils_parents', function (Blueprint $table) {
            $table->unsignedBigInteger('pupil_id');
            $table->unsignedBigInteger('parent_id');

            $table->foreign('pupil_id')
                ->references('id')
                ->on('pupils')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('parent_id')
                ->references('id')
                ->on('parents')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pupils_parents');
        Schema::dropIfExists('parents');
    }
}
