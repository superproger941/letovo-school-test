<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoordsFieldToPupils extends Migration
{
    public function up()
    {
        Schema::table('pupils', function (Blueprint $table) {
            $table->string('coords')->default('');
        });
    }

    public function down()
    {
        Schema::table('pupils', function (Blueprint $table) {
            $table->dropColumn('coords');
        });
    }
}
