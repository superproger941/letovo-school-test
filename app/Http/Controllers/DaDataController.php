<?php

namespace App\Http\Controllers;

use App\Models\Pupil;

class DaDataController extends Controller
{
    public function normalization(Pupil $pupil)
    {
        return response()->json($pupil->enrich());
    }

    public function addCoordsToPupil(Pupil $pupil)
    {
        return response()->json($pupil->addCoordinatesModelAndGet());
    }
}

