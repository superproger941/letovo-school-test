<?php

namespace App\Http\Controllers;

use App\Http\Requests\PupilRequest;
use App\Http\Resources\Pupil as PupilResource;
use App\Models\Pupil;
use Illuminate\Http\Request;

class PupilsController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/pupils",
     *     description="Get all pupils",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *          response="default",
     *          description="Get all pupils",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *     )
     * )
     */
    public function index()
    {
        return PupilResource::collection(Pupil::paginate());
    }

    /**
     * @OA\Get(
     *     path="/api/pupils/{pupil}",
     *     description="Get pupil by id",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="pupil",
     *         in="path",
     *         description="pupil id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *     response="default", description="Get pupil by id",
     *     @OA\MediaType(
     *              mediaType="application/json",
     *          )
     * ),
     *     @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *     )
     * )
     */
    public function show(Pupil $pupil)
    {
        return new PupilResource($pupil);
    }

    /**
     * @OA\Post(
     *     path="/api/pupils",
     *     description="Add pupil",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *     response="default",
     *      description="Add pupil",
     *     @OA\MediaType(
     *              mediaType="application/json",
     *          )
     * ),
     *     @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *     ),
     *     @OA\Response(
     *          response=422,
     *          description="Wrong credentials response",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Sorry, wrong credentials. Please try again")
     *          )
     *     ),
     *     requestBody={"$ref": "#/components/requestBodies/Pupil"},
     * )
     */
    public function store(PupilRequest $request)
    {
        $pupil = Pupil::create($request->all());

        return new PupilResource($pupil);
    }

    /**
     * @OA\Put(
     *     path="/api/pupils/{pupil}",
     *     description="Update pupil",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="pupil",
     *         in="path",
     *         description="pupil id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *     response="default",
     *     description="Update pupil",
     *     @OA\MediaType(
     *              mediaType="application/json",
     *          )
     * ),
     *     @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *     ),
     *     @OA\Response(
     *          response=422,
     *          description="Wrong credentials response",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Sorry, wrong credentials. Please try again")
     *          )
     *     ),
     *     requestBody={"$ref": "#/components/requestBodies/Pupil"},
     * )
     */
    public function update(Request $request, Pupil $pupil)
    {
        return $pupil->update($request->all());
    }

    /**
     * @OA\Delete(
     *     path="/api/pupils/{pupil}",
     *     description="Delete pupil",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="pupil",
     *         in="path",
     *         description="pupil id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *     response="default",
     *      description="Delete pupil",
     *     @OA\MediaType(
     *              mediaType="application/json",
     *          )
     * ),
     *     @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *     ),
     * )
     */
    public function destroy(Pupil $pupil)
    {
        $pupil->delete();

        return response('ok');
    }
}
