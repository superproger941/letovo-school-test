<?php

namespace App\Models;

use Fomvasss\Dadata\Facades\DadataClean;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pupil extends Model
{
    use HasFactory;

    protected $fillable = [
        'fio',
        'phone',
        'email',
        'address',
    ];

    public function enrich()
    {
        return [
            'fio' => DadataClean::cleanName($this->fio),
            'address' => DadataClean::cleanAddress($this->address),
            'phone' => DadataClean::cleanPhone($this->phone),
            'email' => DadataClean::cleanEmail($this->email),
        ];
    }

    public function addCoordinatesModelAndGet()
    {
        $coordsData = DadataClean::cleanAddress($this->address);

        $this->coords = $coordsData->geo_lat . ', ' . $coordsData->geo_lon;

        $this->save();

        return [
            'lat' => $coordsData->geo_lat,
            'lon' => $coordsData->geo_lon,
        ];
    }
}
