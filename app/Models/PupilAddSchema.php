<?php

/**
 * Class Pupil
 *
 * @package Pupil
 *
 * @author  Vlad Grigorev <vadgrigorev94@gmail.com>
 *
 * @OA\Schema(
 *     description="Pupil add model",
 *     title="Pupil add model",
 *     required={"fio", "phone", "email", "address"},
 * )
 */
class Pupil
{
    /**
     * @OA\Property(
     *     description="Username",
     *     property="fio",
     * )
     *
     * @var string
     */
    private $fio;

    /**
     * @OA\Property(
     *     description="Phone",
     *     property="phone",
     * )
     *
     * @var string
     */
    private $phone;

    /**
     * @OA\Property(
     *     description="Email",
     *     property="email",
     *     format="email",
     *     example="user1@mail.com"
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *     description="Address",
     *     property="address",
     * )
     *
     * @var string
     */
    private $address;
}
